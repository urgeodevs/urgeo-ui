import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, share } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { EssaisDialog } from '../essais-dialog/essais-dialog';
declare let L;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      share()
    );

  toppings = new FormControl();
  toppingList: string[] = ['Sismique', 'Electrique', 'Forage', 'Autre 1', 'Autre 2', 'Autre 3'];

  constructor(public dialog: MatDialog, private breakpointObserver: BreakpointObserver) { }

  ngOnInit() {
    // var grayscale = L.tileLayer(mapboxUrl, {id: 'MapID', attribution: mapboxAttribution});
    // var streets   = L.tileLayer(mapboxUrl, {id: 'MapID', attribution: mapboxAttribution});

    //OSM tiles attribution and URL
    var osmLink = '<a href="http://openstreetmap.org">OpenStreetMap</a>';
    var osmURL = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
    var osmAttrib = '&copy; ' + osmLink;


    //Stamen Toner tiles attribution and URL
    var stamenURL1 = 'http://stamen-tiles-{s}.a.ssl.fastly.net/terrain/{z}/{x}/{y}.jpg';
    var stamenAttrib1 = 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>';

    //Stamen Toner tiles attribution and URL
    var stamenURL2 = 'http://stamen-tiles-{s}.a.ssl.fastly.net/watercolor/{z}/{x}/{y}.jpg';
    var stamenAttrib2 = 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>';

    //Creation of map tiles
    var osmMap = L.tileLayer(osmURL, { attribution: osmAttrib });
    var stamenMap1 = L.tileLayer(stamenURL1, {
      attribution: stamenAttrib1,
      subdomains: 'abcd',
      minZoom: 0,
      maxZoom: 20,
      ext: 'jpg'
    });

    var stamenMap2 = L.tileLayer(stamenURL2, {
      attribution: stamenAttrib2,
      subdomains: 'abcd',
      minZoom: 0,
      maxZoom: 20,
      ext: 'jpg'
    });

    //Map creation
    const map = L.map('map', {
      layers: [osmMap]
    }).setView([18.80, -72.50], 8.45);

    //Base layers definition and addition
    var baseLayers = {
      "Default": osmMap,
      "Terrain": stamenMap1,
      "Watter Color": stamenMap2
    };

    //Add baseLayers to map as control layers
    L.control.layers(baseLayers).addTo(map);
    // L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    //     attribution: '© <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    // }).addTo(map);

    setTimeout(() => {
      this.createCircle(18.80, -72.50, map);
      this.createCircle(19.457182, -72.676112, map);
      this.createCircle(19.073367, -72.072455, map);
      this.createCircle(18.843783, -72.457608, map);
    }, 1000)
  }

  createCircle(lat: number, long: number, map) {
    var circle = L.circle([lat, long], {
      color: 'red',
      fillColor: '#f03',
      fillOpacity: 0.5,
      radius: 5000
    }).addTo(map).on('click', ()=>{
      this.dialog.open(EssaisDialog, {
        width: "300",
        height: "400px",
        data: ""
      });
    });

  }

  openDialog() {

  }
}
