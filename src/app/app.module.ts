import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppNavComponent } from './app-nav/app-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { EssaisDialog } from './essais-dialog/essais-dialog'
import { MapComponent } from './map/map.component';

import { GenericProvider } from './providers/generic';

import { appRoutes } from './app-routing.module';

import {
  MatToolbarModule,
  MatButtonModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatTreeModule,
  MatInputModule,
  MatSelectModule,
  MatDialogModule,
  MatCheckboxModule,
 } from '@angular/material';

@NgModule({
  declarations: [
    AppComponent,
    AppNavComponent,
    EssaisDialog,
    MapComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    HttpClientModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatCheckboxModule,
    MatListModule,
    MatTreeModule,
    MatInputModule,
    MatSelectModule,
  ],
  providers: [GenericProvider],
  entryComponents:[
    EssaisDialog, 
    ],
  bootstrap: [AppComponent],
})
export class AppModule { }
