import { User } from '../models/models';


export class AppSettings {
	//public static URL_BASE = "http://192.168.43.47:8080/";	  
	public static URL_BASE = "http://localhost:8080/urgeolab/api";
	public static LANGUAGE = "ENG";
	public static CURENCY_CHANGE = "HTG";
	public static IS_LOGIN: boolean = false;
	public static IS_REGISTERED: boolean = true;
	public static DEFAULT_USER: User = {
		userId:"100",
		userPrivilege:1,
		userFirstname:"Tizon",
		userLastname:"Dife",
		userTitle:"Default",
		userSex:"F",
		userPhone:"0000",
		userEmail:"info@lae.ht",
		userUsername:"TizonDife",
		userPassword:"12344321",
		userCode:"509",
		userImg:"../../assets/img/user.png",
		userDetails:"ZeroAccess",
		userActive:true,
		createdBy:"TizonDife",
		dateCreated:"2017-10-10",
		modifiedBy:"None",
		dateModified:"2017-10-10"
	};
}
