import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';

@Component({
  selector: 'essais-dialog',
  templateUrl: './essais-dialog.html',
  styleUrls: ['./essais-dialog.css']
})
export class EssaisDialog implements OnInit {

  constructor(public dialogRef: MatDialogRef<EssaisDialog> ,public dialog: MatDialog){ 

  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
  }

}