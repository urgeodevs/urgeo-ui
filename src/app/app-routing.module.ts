import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MapComponent } from './map/map.component';


export const appRoutes: Routes = [
  { 
    path: 'home', 
    component: MapComponent 
  },

  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  { path: '**', component: MapComponent }
];