import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, share } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';

import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { MatDialog } from '@angular/material/dialog';

interface FoodNode {
  name: string;
  children?: FoodNode[];
}

const TREE_DATA: FoodNode[] = [
  {
    name: 'Essais',
    children: [
      { name: 'Sismique' },
      { name: 'Electrique' },
      { name: 'Forage' },
    ]
  }, {
    name: 'Rapports',
    children: [
      {
        name: 'Rap1',
        children: [
          { name: 'Rap11' },
          { name: 'Rap12' },
        ]
      }, {
        name: 'Rap2',
        children: [
          { name: 'Rap21' },
          { name: 'Rap22' },
        ]
      },
    ]
  },
];

@Component({
  selector: 'app-app-nav',
  templateUrl: './app-nav.component.html',
  styleUrls: ['./app-nav.component.css']
})
export class AppNavComponent implements OnInit {

  treeControl = new NestedTreeControl<FoodNode>(node => node.children);
  dataSource = new MatTreeNestedDataSource<FoodNode>();

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      share()
    );

  toppings = new FormControl();
  toppingList: string[] = ['Sismique', 'Electrique', 'Forage', 'Autre 1', 'Autre 2', 'Autre 3'];

  constructor(public dialog: MatDialog, private router: Router, private breakpointObserver: BreakpointObserver) {
    this.dataSource.data = TREE_DATA;
  }

  hasChild = (_: number, node: FoodNode) => !!node.children && node.children.length > 0;

  ngOnInit() {

  }

  // openDialog(): void {
  //   let dialogRef = this.dialog.open(DialogUserUpdate, {
  //     width: "250px",
  //     height: "400px",
  //     data: {}
  //   });

  //   dialogRef.afterClosed().subscribe(result => {
  //     console.log('The dialog was closed');
  //   });
  // }

  goTo() {
    this.router.navigate(["/home"]);
  }
}
