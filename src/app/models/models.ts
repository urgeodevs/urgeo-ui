
export class EssaisType{
    essaisTypeId: number;
    essaisTypeName: string;
    essaisTypeDesc: string;
    essaisTypeDetails: string;
    createdBy: string;
    dateCreated: string;
    modifiedBy: string;
    dateModified: string;
}

export class Institution{
    institutionId: number;
    institutionName: string;
    institutionDesc: string;
    institutionDetails: string;
    createdBy: string;
    dateCreated: string;
    modifiedBy: string;
    dateModified: string;
}

export class Essais{
    essaisId:number;
    essaisAltId:string;
    essaisTypeId:number;
    institutionId:number;
    essaisSiteName:string;
    essaisPosX:number;
    essaisPosY:number;
    essaisPosZ:number;
    essaisDate:string;
    essaisTime:string;
    essaisFile:string;
    essaisImage:string;
    essaisTestWater:boolean;
    essaisTestGranulo:boolean;
    essaisTestSedimento:boolean;
    essaisTestPrevl:boolean;
    essaisComment:string;
    createdBy:string;
    dateCreated:string;
    modifiedBy:string;
    dateModified:string;
}

export class User{
    userId: string;
    userPrivilege: number;
    userFirstname: string;
    userLastname: string;
    userTitle: string;
    userSex: string;
    userPhone: string;
    userEmail: string;
    userUsername: string;
    userPassword: string;
    userCode: string; 
    userImg: string;
    userDetails: string;
    userActive: boolean;
    createdBy: string;
    dateCreated: string;
    modifiedBy: string;
    dateModified: string;
}