import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EssaisDeleteComponent } from './essais-delete.component';

describe('EssaisDeleteComponent', () => {
  let component: EssaisDeleteComponent;
  let fixture: ComponentFixture<EssaisDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EssaisDeleteComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EssaisDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
