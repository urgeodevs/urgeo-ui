import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EssaisAddComponent } from './essais-add.component';

describe('EssaisAddComponent', () => {
  let component: EssaisAddComponent;
  let fixture: ComponentFixture<EssaisAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EssaisAddComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EssaisAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
