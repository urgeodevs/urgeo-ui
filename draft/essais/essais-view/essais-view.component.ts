
import { Component, ElementRef, ViewChild, Inject, Input, OnInit } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { MatPaginator, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';

import { Essais } from '../../models/models';
import { GenericProvider } from '../../providers/generic';

import { EssaisAddComponent } from '../essais-add/essais-add.component';

@Component({
  selector: 'app-essais-view',
  templateUrl: './essais-view.component.html',
  styleUrls: ['./essais-view.component.css']
})
export class EssaisViewComponent implements OnInit {
  displayedColumns = ['Code', 'Coordonees', 'Site', 'Institution', "Details", "Update"];
  audDatabase = new EssaisDatabase(this.gProvider);
  dataSource: EssaisDataSource | null;

  @ViewChild('filter') filter: ElementRef;

  constructor(private gProvider: GenericProvider, public dialog: MatDialog) {

  }

  fetchData(): void {
    this.audDatabase = new EssaisDatabase(this.gProvider);
    this.dataSource = new EssaisDataSource(this.audDatabase);
  }

  ngOnInit() {
    this.dataSource = new EssaisDataSource(this.audDatabase);
    Observable.fromEvent(this.filter.nativeElement, 'keyup')
      .debounceTime(150)
      .distinctUntilChanged()
      .subscribe(() => {
        if (!this.dataSource) { return; }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
  }


  openDetailsDialog(param): void {
    let data: any = {};
    data.aud = param[0];
    data.aff = param[1];

    let dialogRef = this.dialog.open(EssaisDetails, {
      width: '850px',
      height: '650px',
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The essais dialog was closed');
      this.fetchData();
    });
  }

  openUpdateDialog(param): void {

    let dialogRef = this.dialog.open(EssaisAddComponent, {
      width: '950px',
      height: '750px',
      data: param
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The essais dialog was closed');
      this.fetchData();
    });
  }
}

export class EssaisDatabase {

  dataChange: BehaviorSubject<Essais[]> = new BehaviorSubject<Essais[]>([]);

  get data(): Essais[] {
    return this.dataChange.value;
  }

  constructor(private gProvider: GenericProvider) {
    this.gProvider.getData("/essais/all").subscribe(data => {
      console.log("data essais: ", data);
      this.dataChange.next(<Array<Essais>>data);
    }, error => {
      console.log("data error: ", error);
    })
  }
}

export class EssaisDataSource extends DataSource<any> {
  _filterChange = new BehaviorSubject('');
  get filter(): string { return this._filterChange.value; }
  set filter(filter: string) { this._filterChange.next(filter); }

  constructor(private _Database: EssaisDatabase) {
    super();
  }

  connect(): Observable<Essais[]> {
    const displayDataChanges = [
      this._Database.dataChange,
      this._filterChange,
    ];

    return Observable.merge(...displayDataChanges).map(() => {
      return this._Database.data.slice().filter((item: Essais) => {
        let searchStr = (item[0].audienceDetails + item[0].dateCreated + item[1].affaireId + "").toLowerCase();
        return searchStr.indexOf(this.filter.toLowerCase()) != -1;
      });
    });
  }

  disconnect() { }
}

@Component({
  selector: 'essais-details',
  templateUrl: './essais-details.html',
  styleUrls: ['../../affaire.css'],
  providers: [GenericProvider]
})
export class EssaisDetails {
  aud: Essais;
  aff: Essais;

  iname = "";


  constructor(@Inject(MAT_DIALOG_DATA) public data: any, public dialogRef: MatDialogRef<EssaisDetails>, public formBuilder: FormBuilder, private gProvider: GenericProvider) {
    this.iname = "URGEO";

    this.aff = this.data.aff;
    this.aud = this.data.aud;

    console.log("data found for details essais: ", this.data);

    // if(this.aff != null && this.aff != undefined){
    //   //lawer adverse
    //   this.gProvider.getData("/person/" + this.aff.advocatAdvId).subscribe(data=>{
    //     let obj: any = data;
    //     this.lawer_adv = <Person>obj;
    //     console.log("lawer adverse for details: ", this.lawer_adv);
    //   }, error=>{
    //     console.log("error getting lawer adverse for details: ", error);
    //   });

    //   //lawer for
    //   this.gProvider.getData("/person/" + this.aff.audienceLawerId).subscribe(data=>{
    //     let obj: any = data;
    //     this.lawer_for = <Person>obj;
    //     console.log("lawer for for details: ", this.lawer_adv);
    //   }, error=>{
    //     console.log("error getting lawer for for details: ", error);
    //   });
    // }else{
    //   console.log("can't find all data for details essais: ");
    // }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  printTch(): void {
    let printContents, popupWin;
    printContents = document.getElementById('print-section').innerHTML;
    popupWin = open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title> Institution ${this.iname}</title>
          <style>

          .iheader{
            background: #ddd;
            padding: 10px;
            margin-bottom: 20px;
          }
        
          .lab{
            padding: 10px 4px;
          }

          .lab label{
            color: #555;
          }

          table td, table td * {
              vertical-align: top;
          } 
          
          .tdd{
            border-left: solid 1px #eee;
          }
		</style>
        </head>
       <body onload="window.print();window.close()">
          <br/> <br/>
  
        </body>

        <footer>

        <hr>
        La Direction
        </footer>
      </html>`
    );
    popupWin.document.close();
  }

}
