import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EssaisViewComponent } from './essais-view.component';

describe('EssaisViewComponent', () => {
  let component: EssaisViewComponent;
  let fixture: ComponentFixture<EssaisViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EssaisViewComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EssaisViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
